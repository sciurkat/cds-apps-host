# CdsAppsHost

This project was generated using [Nx](https://nx.dev).

## Quick Start & Documentation

Add `127.0.0.1 support.test.tbint.rginger.com` to hosts file (C:\Windows\System32\drivers\etc\hosts)

Run `npm start start` and open [http://support.test.tbint.rginger.com:4400/](http://support.test.tbint.rginger.com:4400/)

All settings are currently in `\cds-apps-host\apps\apps-host\src\app\tb.service.ts`

