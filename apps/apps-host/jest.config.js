module.exports = {
  name: 'apps-host',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/apps-host',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
