import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

const tbDomain =
  'https://support.neonusdev-astack-tumorboard.tbint.rginger.com';
const appUrl = 'http://support.test.tbint.rginger.com:4500';
const appId = 2;
// TODO
const context = {
  patient: {
    dateOfBirth: '1986-01-01',
    gender: 'M',
    uuid: '9dfd4505-ea6a-4e23-9900-a01eed0f00bc'
  },
  biomarkers: [],
  cancerStaging: {
    clinical: {
      m: null,
      n: null,
      t: null,
      stage: null,
      codedInfo: {
        tCodedTerm: null,
        nCodedTerm: null,
        mCodedTerm: null,
        stageCodedTerm: null
      }
    },
    pathological: {
      m: null,
      n: null,
      t: null,
      stage: null,
      codedInfo: {
        tCodedTerm: null,
        nCodedTerm: null,
        mCodedTerm: null,
        stageCodedTerm: null
      }
    }
  },
  tumorInfo: {
    type: null,
    location: null,
    description: null,
    others: [],
    codedInfo: {
      location: {
        termCode: null,
        termDescription: null,
        localTerm: null,
        system: null,
        version: null,
        retired: null,
        preferred: null
      },
      cancerType: {
        termCode: null,
        termDescription: null,
        localTerm: null,
        system: null,
        version: null,
        retired: null,
        preferred: null
      }
    }
  },
  genomics: [],
  geolocation: {
    address: '',
    postalCode: '',
    country: '',
    navifyId: 'support--'
  },
  config: { actions: { pin: true } },
  fhir: {
    fhirApiUrl: 'https://api.appdev.platform.rginger.com/neonusdev/fhirapi',
    systemId: 'http://neonusdev.tumorboard.navify.com',
    appToken:
      'eyJrZXlpZCI6ImZoaXItYXBpLWtleS0yMDE5LTA1LTI5IiwiYWxnIjoiQTI1NktXIiwiZW5jIjoiQTEyOENCQy1IUzI1NiJ9.TpNvL91Fw4OESrv3ERcrUiYa7euWgSJw4f_lB0N4VlXxqttcWUgPcA.tetaZ8XQAMWMEGBXUyVsyg.MMfD4fVrmt3y71munALk5nXpSNWkdb-RwdvXL1r9IMAIKvMhmvFMizWZ2kOr1ZWjlX9a_NhYeAPLTttmLe3jrqg_rnUCvNn34K2ygHbm05_eD1YabEAITasHz2NGmIVOXaE6JU-p1k0xlM5g7GLao_rPOrMWdDQT8LDl8zADY3HEFBjaRwzoaWjRFmyluAHtkhZCmAyTWQcZQU2MvuqVxMQ35ZcsInXUCJOdBITn6GwF_vCB2Uv6MH1dY9oIFulwz8QhwZIp0zH_gd8FjLYeA_HBzNwQGx6AyZJz_5U0RcOkvYMCqh6UXW3NJ6gC98K0dAgJG5-578IItKc3ua5ASKCwfU5ijjMAtv1W6d9td5A_mQd01miwkNyNNmH7Dt476XnCpQzUpukxOOgBy_4IdOJQzVTfcR1U5q4SI4TyvsnoyiUOmtPf-akV9e7tSOFryTp2-JrZPq2LHIdvn-qbP7Xanv0hzUcNqz0R0sj0d_Ma1RiyTD_8mJ9LN2rZCrXJ.ZOCfoLBrQNLLqNpnpJt5FA'
  },
  oktaId: '00u4o6bbvAeNZkXKL296'
};

@Injectable({
  providedIn: 'root'
})
export class TbService {
  uiConfig;

  constructor(private http: HttpClient) {}

  getAppUrl() {
    return appUrl;
  }

  getContext() {
    return context;
  }

  getAuthorizationHeader() {
    const url = `${this.uiConfig.api.baseUrl}/v1/token/cds-apps?app-id=${appId}`;
    const httpOptions: Object = {
      responseType: 'text',
      headers: new HttpHeaders({
        'X-Navify-Tenant': 'support',
        'X-TB-Authorization': ''
      })
    };
    return this.http.get(url, httpOptions);
  }

  getUiConfiguration() {
    return this.http
      .get(`${tbDomain}/tumor-board/api/v1/ui-configuration`)
      .pipe(
        map(config => {
          const uiConfig = this.formUiConfig(config);
          this.uiConfig = uiConfig;
          return uiConfig;
        })
      );
  }

  private formUiConfig(uiConfig) {
    const apiGatewayURL = this.ensureProtocol(
      uiConfig.platform.apiGatewayURL || ''
    );
    const apiGatewayBasePath = this.fixPathname(
      uiConfig.platform.apiGatewayBasePath || ''
    );
    const tumorboardAppAlias =
      uiConfig.platform.tumorboardAppAlias || 'tumorboard';
    const tumorboardTenantAlias = this.getTenantFromHost();
    return {
      appUuid: uiConfig.platform.appUuid || '',
      appDynamics: uiConfig.appDynamics,
      walkMe: uiConfig.walkMe,
      forgottenPasswordUrl: uiConfig.forgottenPasswordUrl || '',
      sessionTimeout:
        uiConfig.sessionTimeoutIntervalInMinutes * 60 * 1000 || 30 * 60 * 1000,
      tumorboardAppAlias,
      auth: uiConfig
        ? {
            provider: {
              type: 'okta',
              url: uiConfig.platform.oktaUrl || '',
              clientId: uiConfig.platform.oktaClientId || ''
            },
            platform: {
              url: apiGatewayURL
            },
            app: {
              url: this.ensureProtocol(uiConfig.platform.authUiUrl || '')
            },
            client: {
              appAlias: tumorboardAppAlias,
              tenantAlias: tumorboardTenantAlias
            }
          }
        : null,
      api: uiConfig
        ? {
            platformBaseUrl: apiGatewayURL + '/platform',
            baseUrl: apiGatewayURL + apiGatewayBasePath + '/tumor-board/api'
          }
        : null
    };
  }

  private ensureProtocol(url) {
    const URL_SCHEMA_CHECK_REGEXP = /^[a-z]+:\/\//;
    if (URL_SCHEMA_CHECK_REGEXP.test(url)) {
      return url;
    }

    const { protocol } = window.location;
    return `${protocol}//${url}`;
  }

  private fixPathname(pathname) {
    const PATHNAME_TRIM_REGEXP = /^[/]*|[/]*$/g;
    return '/' + pathname.replace(PATHNAME_TRIM_REGEXP, '');
  }

  private getTenantFromHost() {
    const { hostname } = window.location;
    const [tenantId] = hostname.split('.');

    return tenantId;
  }
}
