import { ReceiveMessage } from '@dia/app-plugin/core';
import { PlatformAPI } from '@dia/app-platform-api';

@ReceiveMessage
export class AppPluginApi extends PlatformAPI {
  constructor(public component: any) {
    super();
  }

  getAuthorizationHeader() {
    return new Promise((resolve, reject) => {
      this.component.tb.getAuthorizationHeader().subscribe(token => {
        resolve(token);
      });
    });
  }

  // TODO
  getContext() {
      console.log('getContext');
      return new Promise((resolve, reject) => {
          resolve(this.component.tb.getContext());
      //     this.cdsAppComponent.requestContext().then((context) => {
      //         resolve(context);
      //     }, () => {
      //         reject();
      //     });
      });
  }

  // TODO
  // selectContent(content) {
  //     return new Promise((resolve, reject) => {
  //         // this.cdsAppComponent.pinAppContent(content).then((isPinned) => {
  //         //     resolve(isPinned);
  //         // }, () => {
  //         //     reject();
  //         // });
  //     });
  // }

  // isContentSelected(contentIds) {
  //     return new Promise((resolve, reject) => {
  //         // this.cdsAppComponent.isContentPinned(contentIds).then((isPinnedArray) => {
  //         //     resolve(isPinnedArray);
  //         // }, () => {
  //         //     reject();
  //         // });
  //     });
  // }

  // shareContent(content) {
  //     // content;
  //     // return new Promise((resolve) => {
  //     //     resolve();
  //     // });
  // }

  // pushRoute(route) {
  //     // this.cdsAppComponent.pushRoute(route);
  // }

  // dispose() {
  //     this.context = null;
  // }
}
