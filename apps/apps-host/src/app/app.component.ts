import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { forkJoin } from 'rxjs';
import { Auth } from '@dia/auth';
import { AppPluginApi } from './app-plugin-api';
import { TbService } from './tb.service';

@Component({
  selector: 'cds-apps-host-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  auth;
  api: AppPluginApi;
  appUrl;

  constructor(
    private tb: TbService,
    private sanitizer: DomSanitizer
  ) {
    this.appUrl = sanitizer.bypassSecurityTrustResourceUrl(this.tb.getAppUrl());
    this.initAuth();
    this.initApi();
  }

  initIframe(iframe) {
    this.api.attachMessenger(iframe.contentWindow);
  }

  private initAuth() {
    this.tb.getUiConfiguration()
      .subscribe(uiConfig => {
        this.auth = new Auth(uiConfig.auth, { redirectUrlUseHash: true });

        this.auth.init().then(() => {
          forkJoin(          
            this.auth.getSession(),
            this.auth.getLoginReturn()
          ).subscribe(([session, loginResult]) => {
            if (session) {
              this.handleLogin(session, loginResult);
            } else {
                if (!loginResult) {
                    this.doLogin();
                }
            }
          })
      });
    });
  }

  private initApi() {
    this.api = AppPluginApi(this);
  }

  private doLogin() {
    const returnTo = window.location.href;
    const state = null;
    this.auth.loginRedirect({returnTo, state});
  }

  private handleLogin(session, loginResult) {
    setInterval(() => this.auth.refreshSession(), 10 * 60 * 1000);
  }
}
