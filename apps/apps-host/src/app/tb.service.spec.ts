import { TestBed } from '@angular/core/testing';

import { TbService } from './tb.service';

describe('TbService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TbService = TestBed.get(TbService);
    expect(service).toBeTruthy();
  });
});
